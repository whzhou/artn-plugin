## Platform information:
<!-- This is to help replicate the issue as closeley as possible !-->
- plugin-ARTn version:
- energy/force engine used, version:
- Operating system:
- Compilation details from the file `environment_variables`:
  - Compilers (variables `F90`, `CXX`, and `CC`): 
  - Libraries (variables `BLAS_LIB` and `FORT_LIB`):


## Actual behaviour:
<!-- A brief description of the actual behaviour. !-->
<!-- Paste the entire error message you obtain (if any). !-->

## Expected behaviour:
<!-- Brief description of the expected behaviour. !-->

## Steps to reproduce:
<!-- List the relevant steps to reproduce the error (paste the exact input files/commands you used). !-->

## Further information:
<!-- Files, links, etc. !-->
