.. _variable_groups:

###################
Groups of variables
###################

The variables of pARTn are divided into three groups:

* :ref:`Group ``param``<group_param>`
* :ref:`Group ``data``<group_data>`
* :ref:`Group ``runparam``<group_runparam>`


.. _group_param:
Group ``param``
================

.. doxygengroup:: group_params
   :project: plugin-ARTn


.. _group_data:
Group ``data``
==============

.. doxygengroup:: group_data
   :project: plugin-ARTn


.. _group_runparam:
Group ``runparam``
===================

.. doxygengroup:: group_runparams
   :project: plugin-ARTn


