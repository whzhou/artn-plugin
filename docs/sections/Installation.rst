.. _installation:

Installation
============

.. toctree::
    :maxdepth: 1
    
    qe
    lammps
    install_siesta

..    install_QE
..    install_LAMMPS
