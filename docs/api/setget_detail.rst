.. _setget_detail:


The ``set_*`` and ``get_*`` functions
=====================================

.. note::

   If you are not completely sure if you should use these functions or not, read :ref:`this page <setextract_interf>` first!


.. doxygengroup:: setget_param
   :project: plugin-ARTn

.. doxygengroup:: setget_data
   :project: plugin-ARTn

.. doxygengroup:: setget_runparam
   :project: plugin-ARTn
