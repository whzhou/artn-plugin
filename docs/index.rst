.. plugin-ARTn documentation master file, created by
   sphinx-quickstart on Wed Dec 21 13:42:19 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Plugin-ARTn (pARTn) documentation!
=======================================

This is the online documentation for the plugin-ARTn (pARTn) software. The reference publication is [poberznik2024]_.

..
   See also [jay2020]_.
   Reference to Mousseau webpage `ARTn <http://normandmousseau.com/>`_


..
   The Doxygen file is `here <./_static/index.html>`_

.. toctree::
   :maxdepth: 1
   :caption: User Guide

   sections/Intro
   sections/Installation
   sections/Input
   sections/Output
   sections/troubleshoot
   sections/Ex
..   sections/Examples
..   sections/Introduction


.. toctree::
   :maxdepth: 1
   :caption: Programmer Guide

   sections/philosophy
   sections/extensions
   group/Code_organization
   src/index
   sections/interfaces
   params/index
   sections/API_usage



**********
References
**********

.. target-notes::

.. [barkema1996] G.T. Barkema, N. Mousseau, https://doi.org/10.1103/PhysRevLett.77.4358
.. [malek2000] R. Malek, N. Mousseau, https://doi.org/10.1103/PhysRevE.62.7723
.. [kallel2010] H. Kallel, et al., https://doi.org/10.1103/PhysRevLett.105.045503
.. [marinica2011] M.C. Marinica, et al., https://doi.org/10.1103/PhysRevB.83.094119
.. [trochet2015] M. Trochet, et al., https://doi.org/10.1103/PhysRevB.91.224106
.. [jay2020] A. Jay, et al., https://doi.org/10.1021/acs.jctc.0c00541
.. [poberznik2024] M. Poberznik, et.al., https://doi.org/10.1016/j.cpc.2023.108961
